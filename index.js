// var express = require('express');
"use strict";
import express from 'express';
import bodyParser from 'body-parser';
import pomodoroRoutes from './pomodoro_routes';

let app = express();

import mongoose from 'mongoose';
mongoose.connect('mongodb://localhost/pomodoros');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:false
}));

var pomodoroRoute = pomodoroRoutes(app);

// app.get('/', function (req, res){
//     res.json({hello:'hix'});
// });

var server = app.listen(30001, function(){
    console.log('Server running at localhost:30001');
});